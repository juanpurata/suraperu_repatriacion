var TrackerAnalytics = (function () {
    function TrackerAnalytics(campaign) {
        this._categoryGeneral = campaign;
        this.parseLinks();
    }
    TrackerAnalytics.prototype.parseLinks = function () {
        var _this = this;
        $('[data-event]').on('click', function (evt, noEvent) {
            if (noEvent === void 0) { noEvent = false; }
            if (!$(evt.currentTarget).data('event-manual') && !noEvent) {
                _this.sendEvent($(evt.currentTarget));
            }
        });
    };
    TrackerAnalytics.prototype.sendEvent = function (elm) {
        var category;
        var action;
        var label;
        if (elm.data('event') == 'self') {
            label = elm.text();
        }
        else if (elm.data('event') == 'selector') {
            label = $(elm.data('event-selector'), elm).text();
        }
        else {
            label = elm.data('event');
        }
        if (elm.data('event-category')) {
            category = elm.data('event-category');
        }
        else if (elm.parent().data('event-category')) {
            category = elm.parent().data('event-category');
        }
        else if ($('body').data('event-category')) {
            category = $('body').data('event-category');
        }
        else {
            category = this._categoryGeneral;
        }
        if (elm.data('event-action')) {
            action = elm.data('event-action');
        }
        else if (elm.parent().data('event-action')) {
            action = elm.parent().data('event-action');
        }
        else {
            action = $('body').data('event-action');
        }
        this.sendEventCustom(category, action, label);
    };
    TrackerAnalytics.prototype.sendEventCustom = function (category, action, label) {
        ga('send', 'event', category, action, label);
    };
    TrackerAnalytics.prototype.sendPage = function (url) {
        ga('send', 'pageview', url);
    };
    return TrackerAnalytics;
}());
var Pages = (function () {
    function Pages() {
        console.log("Home:constructor");
    }
    Pages.prototype.allActions = function () {
        console.log("Home:allActions");
    };
    Pages.prototype.main = function () {
        console.log("Home:main");
    };
    return Pages;
}());
var CommonController = (function () {
    function CommonController() {
    }
    CommonController.prototype.allActions = function () {
        var isDesktop = window.matchMedia('(min-width: 1024px)').matches;
        var isDesktopNotFull = window.matchMedia('(min-width: 1024px) and (max-width: 1600px)').matches;
        var isMobile = window.matchMedia('(max-width: 768px)').matches;
        var $win = $(window);
        var winHeight = $win.height();
        var $header = $('.js-header');
        var $anchorNav = $(isDesktop ? '.js-sitenav' : '.js-sitenavMob').find('a');
        var $goToDown = $('.js-goToDown');
        this.goToSection($goToDown);
        $(document).on('scroll', this.onScroll);
        var that = this;
        $anchorNav.on('click', function (ev) {
            ev.preventDefault();
            $(document).off('scroll');
            $anchorNav.each(function () {
                $(this).removeClass('is-active');
            });
            $(this).addClass('is-active');
            var target = this.hash, menu = target;
            var $target = $(target);
            if (isDesktopNotFull && ($target.selector == '#solutions' || $target.selector == '#register')) {
                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top
                }, 500, 'swing', function () {
                    if (!isDesktop) {
                        $('.js-sitenavMob').slideUp('fast');
                        $header.removeClass('open-nav');
                    }
                    $(document).on('scroll', that.onScroll);
                });
            }
            else {
                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top - ($header.outerHeight() - 1)
                }, 500, 'swing', function () {
                    if (!isDesktop) {
                        $('body').removeClass('overflow');
                        $('.js-sitenavMob').slideUp('fast');
                        $header.removeClass('open-nav');
                    }
                    $(document).on('scroll', that.onScroll);
                });
            }
        });
        $('.js-collapser').on('click', function (ev) {
            $('.js-sitenavMob').slideToggle('fast', function () {
                $header.toggleClass('open-nav');
                $('body').toggleClass('overflow');
            });
            $('.js-sitenavMob-mob').slideToggle('fast', function () {
                $header.toggleClass('open-nav');
                $('body').toggleClass('overflow');
            });
        });
        $('.js-tabs li').on('click', function () {
            var tab_id = $(this).attr('data-tab');
            $('.js-tabs li').removeClass('is-current');
            $('.tab__content').removeClass('is-current');
            $(this).addClass('is-current');
            $("#" + tab_id).addClass('is-current');
        });
        if (isDesktop) {
            if (page == 0) {
                $('#form_container .form_input').mouseover(function () {
                    console.log('desactivado scroll;');
                    $(window).off("scroll");
                });
                $('#form_container').mouseleave(function () {
                    $(window).scroll(function () {
                        stickyNav();
                    });
                });
                var stickyNavTop = $('.form_container').offset().top;
                var stickySecondContent = $('#repatriation').offset().top - 340;
                var stickyNav = function () {
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > stickySecondContent) {
                        $('.form_container').addClass('active');
                    }
                    else if (scrollTop == 0) {
                        $('.form_container').removeClass('active');
                    }
                    else {
                        $('.form_container').removeClass('active');
                    }
                };
                stickyNav();
                $(window).scroll(function () {
                    stickyNav();
                });
            }
            else if (page == 1) {
                $('.form_container').addClass('active');
            }
            else if (page == 2) {
                $('.form_container').addClass('active');
            }
            $('.arrow-close').on('click', function () {
                $('.form_container').addClass('active');
            });
            $('.arrow-open').on('click', function () {
                $('.form_container').removeClass('active');
            });
        }
        if (isMobile) {
            if (page == 1) {
                $('#form_container').removeClass('active');
            }
            else if (page == 2) {
                $('#form_container').removeClass('active');
            }
            $('.form_container_mobile').on('click', function () {
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            });
            $('.solution-conceptGallery .image-mobile-1').attr('src', './images/productos/edificio-1-mobile.png');
            $('.solution-conceptGallery .image-mobile-2').attr('src', './images/productos/edificio-2-mobile.png');
            $('.solution-conceptGallery .image-mobile-3').attr('src', './images/productos/edificio-3-mobile.png');
            $('.item-carousel').owlCarousel({
                nav: true,
                dots: true,
                autoHeight: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    480: {
                        items: 1
                    }
                }
            });
            $('.js-repatriationCarousel').owlCarousel({
                nav: true,
                autoHeight: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    480: {
                        items: 2
                    }
                }
            });
        }
        if (isMobile) {
            $('.js-repatriationCarousel').owlCarousel({
                nav: true,
                autoHeight: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    480: {
                        items: 2
                    }
                }
            });
        }
        new WOW().init({ boxClass: 'wow',
            animateClass: 'animated',
            offset: 0,
            mobile: true,
            live: true });
        $('#name, #lastname').alpha();
        $('#dni').alpha().numeric();
        $('#telefono').numeric();
        var formValidator = $('form').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                lastname: {
                    required: true,
                    minlength: 2
                },
                email: {
                    required: true,
                    email: true
                },
                telefono: {
                    required: true,
                    number: true,
                    minlength: 7
                }
            },
            messages: {
                name: {
                    required: '*Este campo es obligatorio',
                    minlength: 'Ingresar un nombre correcto'
                },
                lastname: {
                    required: '*Este campo es obligatorio',
                    minlength: 'Ingresar un nombre correcto'
                },
                email: {
                    required: '*Este campo es obligatorio',
                    email: 'Ingresar un email correcto'
                },
                telefono: {
                    required: '*Este campo es obligatorio',
                    minlength: 'Ingresar un Telefono correcto'
                }
            },
            submitHandler: function (form) {
                $(window).off("scroll");
                grecaptcha.execute();
            }
        });
    };
    CommonController.prototype.validateCaptcha = function () {
        var _this = this;
        $('form #sendBtn').attr('disabled', 'disabled');
        var $form = $('form');
        var data = {
            name: $form.find('#name').val(),
            lastname: $form.find('#lastname').val(),
            email: $form.find('#email').val(),
            phone: $form.find('#telefono').val()
        };
        setTimeout(function () {
            _this.sendForm(data);
        }, 1000);
    };
    CommonController.prototype.sendForm = function (data) {
        var tplThanks = '<div class="thanks">' +
            '<h5 class="thanks__title">¡GRACIAS!</h5>' +
            '<p class="thanks__message">Un asesor se pondrá en contacto contigo.</p>' +
            '</div>';
        $.ajax({
            url: 'https://bdohe8orng.execute-api.us-east-1.amazonaws.com/prod/register',
            crossDomain: true,
            method: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            beforeSend: function () {
                $('.form_data button').addClass('active-loader');
                $('body').css('overflow', 'hidden');
            },
            success: function (response) {
                if (response.success) {
                    $('#form')[0].reset();
                    $('form #sendBtn').removeAttr('disabled');
                    $('.form_container .form-sending').addClass('active');
                    $('.form_container .form_message').addClass('active');
                    setTimeout(function () {
                        $('.form_data button').removeClass('active-loader');
                        $('.form_container .form-sending').removeClass('active');
                        $('.form_container .form_message').removeClass('active');
                        $('body').css('overflow', 'auto');
                    }, 5000);
                }
                else {
                    console.log('error');
                }
            }
        });
    };
    CommonController.prototype.goToSection = function (el) {
        var id;
        $(el).on('click', function (ev) {
            ev.preventDefault();
            id = $(this).closest('section').next().attr('id');
            $('html, body').animate({
                scrollTop: $('#' + id).offset().top - ($('.js-header').outerHeight())
            }, 800);
        });
    };
    CommonController.prototype.onScroll = function (ev) {
        var scrollPos = $(document).scrollTop();
        $('.js-sitenav a').each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr('href'));
            if ((refElement.position().top - 74) <= scrollPos && refElement.position().top + refElement.outerHeight() - 74 > scrollPos) {
                $('.js-sitenav a').removeClass('is-active');
                currLink.addClass('is-active');
            }
            else {
                currLink.removeClass('is-active');
            }
        });
    };
    return CommonController;
}());
var Public = {
    initiatedClasses: {}
};
var UTIL = {
    exec: function (controller, action) {
        if (action === void 0) { action = 'allActions'; }
        if (window[controller]) {
            if (Public.initiatedClasses[controller]) {
                var controllerClass = Public.initiatedClasses[controller];
            }
            else {
                var controllerClass = Object.create(window[controller].prototype);
                controllerClass.constructor.apply(controllerClass);
                Public.initiatedClasses[controller] = controllerClass;
            }
            if (controllerClass[action]) {
                controllerClass[action]();
            }
        }
    },
    init: function () {
        var body = $("body");
        var controller = body.data("router-class");
        var action = body.data("router-action");
        this.exec("CommonController");
        this.exec(controller);
        this.exec(controller, action);
    }
};
$(document).on('ready', function () {
    UTIL.init();
    $('.row-mobile .owl-nav .owl-prev').text('');
    $('.row-mobile .owl-nav .owl-next').text('');
    var pathGeneral = window.location.pathname;
    var urlPath = pathGeneral.substr(pathGeneral.lastIndexOf('/'));
    if (urlPath == '/solucion.html') {
        $('.sitenav__item--btn').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({ scrollTop: document.body.scrollHeight }, "slow");
        });
    }
    else if (urlPath == '/fondos.html') {
        $('.sitenav__item--btn').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({ scrollTop: document.body.scrollHeight }, "slow");
        });
    }
});
function onSubmit(token) {
    var c = new CommonController();
    c.validateCaptcha();
}

//# sourceMappingURL=application.js.map
